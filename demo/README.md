# FTP Scenario Demo Example
This demo provides a step-by-step guide on running a file transfer bot in Kubernetes and Argo Workflows.
You can choose between running it in the provided sandbox or in your own Kubernetes cluster. However, the procedure
will be the same for the sandbox, local, or remote cluster deployment.

## Running the example using Kubernetes only
Despite that use of Argo Workflows can save time and be more manageable for running complex scenarios, we can still use
Kubernetes only, since Argo is only an extension for Kubernetes.

Go to the *kubernetes* folder (`cd kubernetes/`) and start
the FTP server, with which the bot will communicate. You can do this using the following commands.
```
kubectl create -f ftp-server-pod.yaml
kubectl create -f ftp-service.yaml
```

Check if the server is running with `kubectl get pods`. The output should look like this:
```
NAME                           READY   STATUS    RESTARTS       AGE
ftp-fauria-pod                 1/1     Running   0              62s
```
Same you can check that the service is running via the `kubectl get svc` command, and the output will be like this:
```
NAME            TYPE        CLUSTER-IP       EXTERNAL-IP   PORT(S)                   AGE
ftp-service     ClusterIP   10.101.103.173   <none>        21/TCP,20/TCP,21100/TCP   2m22s
kubernetes      ClusterIP   10.96.0.1        <none>        443/TCP                   15m34s
```

You can start the FTP bot now if both the server and the service run. Use the similar command as before:
```
kubectl create -f ftp-bot-pod.yaml
```

**Now your simulation is running!** You can check it by executing `kubectl logs ftp-bot-pod`. You should see that the
file *large_file* is uploading to the server repeatedly.

### How to stop it?
You can delete all created pods and services via these commands:
```
kubectl delete pod ftp-fauria-pod
kubectl delete svc ftp-service
kubectl delete pod ftp-bot-pod
```


## Running the example using Argo Workflows
Go to the *argo* folder (`cd argo/`). We will use Argo CLI in
this demo since it's more reliable. However, you can explore Argo UI by running this command:
```
kubectl -n argo port-forward deployment/argo-server 2746:2746
```
By default, UI is not exposed with an external IP. You will find the UI at the address: `localhost:2746`. So if you want
to use UI with your remote cluster, please read
[this part](https://argoproj.github.io/argo-workflows/argo-server/#access-the-argo-workflows-ui) of Argo documentation.

We can run the whole FTP scenario (same as in the previous example) by executing just one command. This is the advantage
of Argo. We can save time and run complex scenarios quickly. You can try it using this command:
```bash
argo submit -n argo --watch ftp-scenario-w-resources.yaml
```

You should get similar output to this one:
```bash
Name:                ftp-scenario-rdb74
Namespace:           argo
ServiceAccount:      unset (will run with the default ServiceAccount)
Status:              Running
Created:             Wed May 17 17:15:24 +0200 (7 seconds ago)
Started:             Wed May 17 17:15:24 +0200 (7 seconds ago)
Duration:            7 seconds
Progress:            0/2

STEP                   TEMPLATE      PODNAME                                    DURATION  MESSAGE
 ● ftp-scenario-rdb74  ftp-scenario                                                         
 └─┬─◷ server          ftp-server    ftp-scenario-rdb74-ftp-server-537840735    7s          
   └─◷ service         ftp-service   ftp-scenario-rdb74-ftp-service-2222727791  7s   
```

This more complex workflow runs the scenario in three steps. First, it creates an FTP server and a service parallelly
and then an FTP bot. The workflow is done when it's all created. The bot and the server are running in the background.

You can check that Argo creates pods and the service:
```bash
kubectl -n argo get pods
kubectl -n argo get svc
```
You should see two pods, `ftp-bot-pod` and `ftp-fauria-pod` and one service `ftp-service`. (There will also be Argo
system pods and services.)

To check the bot's logs and to see if it's working, execute the following command:
```bash
kubectl -n argo logs ftp-bot-pod
```
You should see that the file large_file is uploading to the server repeatedly.

### How to stop it?
You can delete all created pods and services again just with one command:
```
argo submit -n argo --watch delete-ftp-scenario.yaml
```
There is a prepared template with two steps. One deletes pods, and the second one a service. These steps are done in
parallel.

## What's Next?
You can continue to the [examples README](../examples/README.md) to try Argo's features with more complex
examples in the UI.

## Resources from Docker Hub
- [fauria/vsftpd](https://hub.docker.com/r/fauria/vsftpd/)
- [nusncl1/file-transfer-bot](https://hub.docker.com/r/nusncl1/file-transfer-bot)
