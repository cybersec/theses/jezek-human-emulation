# Human Bots Orchestration

This repository was created as a result of my bachelor's thesis at the Faculty of Informatics, Masaryk University.
The main goal of the bachelor thesis is to create a proof-of-concept scenario which will generate network traffic to
emulate human behaviour in a cybersecurity training environment.
The thesis is available [here](https://is.muni.cz/th/aswj0/).

This repository shows examples of user emulation using [Octobot](https://github.com/nus-ncl/OctoBot) orchestrated by
[Kubernetes](https://github.com/kubernetes/kubernetes) and [Argo Workflows](https://github.com/argoproj/argo-workflows),
which is used uder the Apache License Version 2.0. The main part of this repository and the main result of my thesis 
are examples (in the `examples` folder) of modelling complex scenarios using multistep workflows in Argo.

You can try all the examples in a sandbox virtual machine or in your own Kubernetes cluster with installed Argo.

## Repository structure
The folder `demo` contains a simple example of Octobot's FTP bot in Kubernetes and Argo. The demo helps to show the
difference between deploying using Kubernetes only and deploying using Argo Workflows. It also shows the basic
principles of both technologies.

The folder `examples` is the main result of my bachelor's thesis and contains more complex examples of human emulation
via Argo workflows. It's based on a scenario with multiple bots browsing a web page, which is modelled in different
ways by each example. In other words, examples show ways of modelling multistep workflows in Argo.

In the folder `sandbox`, you will find instructions and files necessary for creating a sandbox VM. This sandbox
provides the easiest way to try all the examples from the `demo` and `examples` folders. It comes with k3s (Kubernetes)
and Argo preinstalled, and it contains both the `demo` and the `examples` folders. If you want to set up and run the
sandbox, continue to its [README](sandbox/README.md).

## Software Requirements
You don't need to install all requirements. Choose whether you want to run the examples in a sandbox VM or your own
cluster and follow the corresponding section.

### For Sandbox
To run the sandbox, you will need the following software:
- [VirtualBox](https://www.virtualbox.org) ([Downloads page](https://www.virtualbox.org/wiki/Downloads))
- [Vagrant](https://www.vagrantup.com) ([Installation guide](https://developer.hashicorp.com/vagrant/docs/installation))
- [Ansible](https://www.ansible.com) ([Installation guide](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html))

Additional software will be installed directly into the sandbox by Ansible automatically.

After installation, you can continue to [sandbox README](./sandbox/README.md) for more instructions.

---

### For Own Cluster
You will need a cluster with Kubernetes and Argo installed. These examples were tested with k3s Kubernetes cluster, but
other clusters like k8s or minikube should work, too.

#### Argo installation
Use Argo's [quick start guide](https://argoproj.github.io/argo-workflows/quick-start/) for non-production deployment.
However, it is unsuitable for production deployment, so in this case, follow their official
[installation guide](https://argoproj.github.io/argo-workflows/installation/).

For one example, you will also need Argo Events installed. To do so, please follow their
[quick start guide](https://argoproj.github.io/argo-events/quick_start/).

#### kubectl installation
kubectl is a command-line tool which allows you to run commands against the Kubernetes cluster. The installation guide
is available [here](https://kubernetes.io/docs/tasks/tools/). Note that you have to configure kubectl for your cluster.
Clusters usually provide a `kubeconfig.yaml` file. So you only need to export this as an environment variable.
```bash
export KUBECONFIG=kubecongig.yaml
```

**Note:** If you are unfamiliar with Argo Workflows or its UI, you can check [sandbox README](./sandbox/README.md).
There are sections about Argo CLI and UI.

Then you can continue to [demo's README](demo/README.md) or [examples' README](examples/README.md) to try the examples
out.

---

Examples were tested using the following software:
- [Docker](https://docs.docker.com) 20.10.21, 23.0.1
- [kubectl](https://kubernetes.io/docs/reference/kubectl/) v1.24.10, v1.25.6+k3s1
- [k3s](https://k3s.io) v1.25.6+k3s1
- [Argo Workflows](https://argoproj.github.io/argo-workflows/) v3.4.4
- [Vagrant](https://www.vagrantup.com) 2.3.0, 2.3.4
- [Ansible](https://www.ansible.com) 2.10.8
- [VirtualBox](https://www.virtualbox.org) 6.1.32 r149290 (Qt5.15.3), 7.0.6 r155176 (Qt5.15.3)
- OS: [Ubuntu](https://ubuntu.com) 22.04.1 LTS
- Sandbox OS: [Xubuntu](https://app.vagrantup.com/munikypo/boxes/xubuntu-18.04) from munikypo vagrant repository
