# The Sandbox
This sandbox provides one virtual machine running k3s, Argo Workflows, and Argo Events. Moreover, the machine contains
all examples files. The sandbox runs [munikypo/ubuntu-18.04](https://app.vagrantup.com/munikypo/boxes/ubuntu-18.04)
image. This version was chosen because it is ready for
[Cyber Sandbox Creator](https://gitlab.ics.muni.cz/muni-kypo-csc/cyber-sandbox-creator) and provides GUI necessary for
Argo Workflows visualisations.

## Software requirements
- [VirtualBox](https://www.virtualbox.org) 6.1.32 r149290 (Qt5.15.3)
  ([Downloads page](https://www.virtualbox.org/wiki/Downloads))
- [Vagrant](https://www.vagrantup.com) 2.3.0
  ([Installation guide](https://developer.hashicorp.com/vagrant/docs/installation))
- [Ansible](https://www.ansible.com) 2.10.8
  ([Installation guide](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html))

## Usage
### Working with the VM
To boot up the virtual machine, open a terminal from the folder of this readme and run
```bash
vagrant up
```
Downloading and provisioning may take a few minutes.

**Note:** Vagrant prints an error message that Ansible failed to install. This bug could be resolved by running
`vagrant provision`.

When the provisioning is done and the machine is up, log in through GUI using `vagrant` as **username and password**.
(You may need to log in twice due to the usage of
[munikypo image](https://app.vagrantup.com/munikypo/boxes/ubuntu-18.04).)

Alternatively, you can use the following command to connect to the VM from your terminal.
```bash
vagrant ssh
```
However, some examples involve Argo UI, so you will need GUI.

To quit the terminal of `vagrant ssh`, use the `exit` command.

---

If you want to stop the machine use
```bash
vagrant halt
```

If you want to restart the machine use
```bash
vagrant reload
```

If you want to stop the machine and delete all data use
```bash
vagrant destroy
```

### Aliases in the Sandbox Terminal
You can save time using aliases instead of long commands in this sandbox. See the table below.

| Alias              | Command                                                       |
|--------------------|---------------------------------------------------------------|
| k                  | kubectl                                                       |
| argo-ui            | kubectl -n argo port-forward deployment/argo-server 2746:2746 |
| k-delete-completed | kubectl delete pod --field-selector=status.phase==Succeeded   |
| ks                 | kubeshark                                                     |


### Argo CLI
To try out Argo CLI, the example of FTP scenario is used. The scenario creates one bot and one FTP server. The bot
repeatedly uploads or downloads one file to/from the server. Both the bot and the server are deployed into the cluster
as Kubernetes resource called Pod which contains a container with a specified image.

To deploy this scenario, navigate to the folder demo and then to the subfolder argo (`cd demo/argo/`) in the VM's
terminal. There are two files, one (`ftp-scenario-w-resources.yaml`) creates the scenario (all pods and services in
k3s), and the second one deletes the whole scenario (`delete-ftp-scenario.yaml`).

To create and run the scenario, use the following command.
```bash
argo submit -n argo --watch ftp-scenario-w-resources.yaml
```

You should see similar output, that will be updated until all steps will be completed.
```bash
Name:                ftp-scenario-rdb74
Namespace:           argo
ServiceAccount:      unset (will run with the default ServiceAccount)
Status:              Running
Created:             Wed May 17 17:15:24 +0200 (7 seconds ago)
Started:             Wed May 17 17:15:24 +0200 (7 seconds ago)
Duration:            7 seconds
Progress:            0/2

STEP                   TEMPLATE      PODNAME                                    DURATION  MESSAGE
 ● ftp-scenario-rdb74  ftp-scenario                                                         
 └─┬─◷ server          ftp-server    ftp-scenario-rdb74-ftp-server-537840735    7s          
   └─◷ service         ftp-service   ftp-scenario-rdb74-ftp-service-2222727791  7s   
```
This workflow runs the scenario in three steps. First, it creates an FTP server and a service parallelly
and then an FTP bot. The workflow is done when all components are created. The bot and the server are running in the
background.

You can check that Argo creates pods and the service:
```bash
kubectl -n argo get pods
kubectl -n argo get svc
```
You should see two pods, `ftp-bot-pod` and `ftp-fauria-pod` and one service `ftp-service`. (There will also be Argo
system pods and services.)

**Tip:** You can use alias `k` instead of `kubectl` in this sandbox and save some time.

To check the bot's logs and to see if it's working, execute the following command:
```bash
kubectl -n argo logs ftp-bot-pod
```
You should see that the file _large_file_ is uploading to the server repeatedly.

You can delete all created pods and services via the command:
```bash
argo submit -n argo --watch delete-ftp-scenario.yaml
```
There is a prepared template with two steps. One deletes pods, and the second one a service. These steps are done in
parallel.

### Argo UI
#### 1. Start the UI
Log into VM using username and password `vagrant`. Open the terminal and port-forward the UI via
```bash
kubectl -n argo port-forward deployment/argo-server 2746:2746
```
**Tip:** You can use the alias `argo-ui` instead of the above command in this sandbox.

Open a web browser. Argo UI is accesible on the site:
```
https://localhost:2746
```
Due to the self-signed certificate, you will receive a TLS error, which you must manually approve.

**Note:** Argo UI is sometimes unreliable and could crash. If you experience some issues with it, check logs in
the terminal. An easy fix is to use `kubectl -n argo port-forward deployment/argo-server 2746:2746` (`argo-ui`) again.

**Note:** A browser can cache the UI page from a previous session sometimes, and it can cause issues with loading the
UI correctly. To fix this, try `Ctrl+Shift+R` in your browser to reload the page and empty a cache.

Argo UI opens with a welcome page, which can be closed by clicking on the cross in the top right corner. Then the UI
looks like this:
![Argo UI](./pictures/ui-01.png)

Check the namespace in Argo UI. In case there is _undefined_ or _default_, change it to _argo_, because
Argo has admin privileges only in the argo namespace.

#### 2. Create a Workflow
You can create a workflow in the first tab and, in the second, a workflow template. You can use templates over and over
with different parameters. Creating workflows and templates is a similar process. This example shows how to create
a template.

Click on the `+ CREATE NEW WORKFLOW TEMPLATE` button. Then click on `UPLOAD FILE` and select
`~/demo/argo/ftp-scenario-w-resources.yaml`. After that, click on `+ CREATE`.

You can create a template from `~/demo/argo/delete-ftp-scenario.yaml` the same way.

**Note:** Argo adds five characters to the end of the name of a workflow or template to ensure that the name is unique.
This string is replaced by `xxxxx` in this README.

#### 3. Submit a Workflow

To submit a workflow, go to the first tab. Click `+ CREATE NEW WORKFLOW` and select `ftp-scenario-xxxxx`, then click on
`+ SUBMIT`.

**Note:** You may need to click on the `Continue` button the first time to get the Workflows page.

![Create Workflow](./pictures/ui-02.png)

You should see this graph:
![Workflow Graph](./pictures/ui-03.png)

You can see that two jobs were done in parallel, and after them, the third one. Pods are now created, and the bot is
sending FTP requests to the server.

You can submit `delete-ftp-scenario-xxxxx` the same way. It deletes all pods and services created by the first template.
![Delete Scenario Graph](./pictures/ui-04.png)

## What's Next?
You can continue to the [examples README](../examples/README.md) to try Argo's features with more complex
examples, or you can go to the [demo README](../demo/README.md) to try the difference between Kubernetes and Argo
deployment.

## Resources
### Docker Hub
- [fauria/vsftpd](https://hub.docker.com/r/fauria/vsftpd/)
- [nusncl1/file-transfer-bot](https://hub.docker.com/r/nusncl1/file-transfer-bot)

### Vagrant Cloud
- [munikypo/ubuntu-18.04](https://app.vagrantup.com/munikypo/boxes/ubuntu-18.04)
