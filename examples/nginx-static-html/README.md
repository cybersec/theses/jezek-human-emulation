This folder contains Dockerfile and HTML files to build the image used by web server in the examples.

The image is also available at DockerHub as
[slunickoondra/nginx-static-html](https://hub.docker.com/r/slunickoondra/nginx-static-html).
