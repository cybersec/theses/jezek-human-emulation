# Web Browsing Example
This example presents the capabilities of Argo in human network traffic emulation. It contains several sub-examples of
various Argo features. All sub-examples have the same goal - to create one HTTP server and multiple web-browsing bots
to browse the server. However, they try to reach that goal in different ways.

**Note:** In all YAML files is an attribute called ttlStrategy, which limits the lifetime of the completed workflow. It
prevents the overwhelming of not running workflows by deleting them. However, it also deletes logs. This limit is set
to 5 minutes (300 seconds). Therefore, each workflow is deleted 5 minutes after its completion. If you want to see logs
for a longer time, change the limit in a specific file here:
```yaml
ttlStrategy:
  secondsAfterCompletion: 300
```

## Prerequisites
If you don't know, how to work with Argo, check [the sandbox README](../sandbox/README.md), where you will find 
an essential Argo CLI and UI guide.

Argo has admin privileges only in the argo namespace, so check that the argo namespace is selected in the UI.

All example files are in the folder of this readme. In the sandbox, it's located in `~/examples`.

## Overview
All the following examples describe the same scenario that is multiple bots browsing one web. The first example creates
both the web server and bots and runs bots in parallel. The others focus of different approaches to bots deployment, so
web server must be deployed separately, or used from the previous example.

The second example creates recursive for-loop to deploy bots sequentially, the third one creates scheduled cron workflow
to deploy bot every minute, and the last one uses Argo Events to extends possibilities of scheduling and deploy bot
every ten seconds.

Every workflow, whether created manually or automatically using a schedule, can be managed via the UI in the Workflows
tab.

Completed workflows are deleted automatically after 5 minutes, and they delete completed bots, too. However, the web
server persists in the cluster. It can be deleted by submitting `delete-web-server.yaml` as a workflow.

## Argo Workflows and Kubernetes Resources
This example creates a scenario with a server and bots in one workflow. These bots browse a web, they click on links
and go to different pages. After browsing the whole page, they stop, and change their status to _Completed_.

The workflow uses Argo's feature to create the server directly from manifest for Kubernetes. This feature is called
workflow with Kubernetes resources. The main advantage of this approach is that the step creating the server can get to
the _Completed_ status even though it is still running. It is because Argo marks the step as completed as soon as the
server is created.

### 1. Create the Workflow Template

Create a new workflow template from the file `web-scenario-w-resources.yaml`.

![Create workflow template](./pictures/img-01.png)

### 2. Submit the Workflow
Create a new workflow from the template _web-scenario-xxxxx_. You can specify how many bots you want to run. The default
value is 5.

![Create workflow](./pictures/img-02.png)

The workflow creates a server and a service in parallel. Then it runs multiple bots, which browse a web page on
the server.

![Running workflow](./pictures/img-03.png)

You can check the logs of each bot by selecting the bot in a graph and then clicking the `LOGS` button.


## Argo Workflows and For Loop
This example is similar to the previous one but creates bots in a for-loop. So they don't run in parallel.

If you have skipped the previous example, upload and submit `web-server.yaml` as a workflow to create a web server
in your cluster. Otherwise, the first example have already created the web server.

How to create a web server (if it was not created in the first example):
![Server workflow](./pictures/img-04.png)

### 1. Create and Submit the Workflow
Create a new workflow template from the file `web-bot-loop.yaml`. The procedure is the same as in the first
example. Create a new workflow from the template _web-loop-xxxxx_. You can specify how many bots you want to run.
The default value is 5. The workflow creates a server and a service in parallel. Then it runs multiple bots, which
browse a web page on the server.

Visualisation of the worklfow:
![For loop workflow](./pictures/img-05.png)

### 2. View Logs
You can check the logs of each bot. Logs contain output of Scrapy library that shows which web pages the bot visits.

![Web bot logs](./pictures/img-06.png)


## Argo Cron Workflows
Cron workflows allows to schedule and thus automate a workflow creation for a specific time
(e.g. every 5th day of a month). To set the schedule standard Cron syntax is used. The shortest interval is denoted
in cron by five asterisks and means "every minute". This example creates a workflow with one web bot every minute
forever.

If you have skipped the first example, run `web-server.yaml` as a workflow to create a web server in your cluster. 

### 1. Create the Cron Worflow
Create new **cron** workflow template in **Cron Workflows tab** from file `web-bot-cron.yaml`.

![Cron workflow](./pictures/img-07.png)

### 2. Submit the Workflow
Submit the workflow using the provided button. After submitting the cron workflow will create a new workflow with
the bot every minute forever. Created Workflows can be viewed in the Workflows tab.

![Created workflows](./pictures/img-08.png)

### 3. Suspend the Cron Workflow
The cron workflow can be suspended in the Cron Workflows tab by selecting the cron workflow and clicking on
the Suspend button. This pause the creation of new workflows.

## Argo Events
Argo Events extends the possibilities of Argo Workflows with event-based scheduling. This example shows how to create
the bot every 10 seconds automatically.

If you haven't already created a web server in previous examples, run `web-server.yaml` as workflow.

### 1. Create the Event Source
The event source is a component transmitting a signal. There are many types of event sources. You can find lots of
examples in [Argo GitHub repository](https://github.com/argoproj/argo-events/tree/master/examples/event-sources).
However, in this case, the most straightforward event source is used. It's called a calendar, which transmits a signal
every 10 s (the interval could be changed).

Create the event source from the file `event-source.yaml`.

![Create event source](./pictures/img-09.png)

### 2. Create the Sensor
The second component is called the sensor. It can receive a signal from an event source and trigger a workflow(s).
In this example, the sensor triggers bot creation (and updates the log) after receiving a signal from the calendar.

Create the sensor from file `web-bot-events-sensor.yaml`.

![Create event sensor](./pictures/img-10.png)

### 3. See Visualisation of Event Flow
Go to the _Event Flow_ tab to watch the whole scenario. You can see a graph with source, sensor, log and workflows with
web bots. Moreover, workflows created by Argo Events can be viewed in Workflows tab.

![Event Flow](./pictures/img-11.png)

### 4. Stop the Argo Events Workflow
You can stop it by deleting the source (calendar) or the sensor. After deletion, new worklfow won't be created, but
already running workflow will continue.

## Resources from Docker Hub
- [slunickoondra/nginx-static-html](https://hub.docker.com/r/slunickoondra/nginx-static-html)
- [nusncl1/web-browsing-bot](https://hub.docker.com/r/nusncl1/web-browsing-bot)
